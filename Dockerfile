FROM node:10-jessie

WORKDIR /usr/src/app

COPY package.json .

RUN npm install
RUN npm i -g supervisor

CMD [ "npm", "run", "dev" ]