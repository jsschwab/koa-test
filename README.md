
### Create a Docker Machine

#### http://myapp.local:3000

```docker-machine create --driver=parallels --parallels-cpu-count=2 --parallels-memory=2048 myapp.local```

### Bring Up the Containers
```docker-compose up -d```

### Connect to the DB
```
postgresql://myapp.local:5432/todos
user: todoapp
```

### Load some stuff

```
http://myapp.local:3000/todos
http://myapp.local:3000/todos/1
```

### Add a new one

In postman, send the following post body to `http://myapp.local/todos/new`:

```
{
	"title": "new one"
}
```



