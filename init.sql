CREATE TABLE public.todos
(
    id SERIAL PRIMARY KEY,
    title VARCHAR(256) NOT NULL,
    complete BOOLEAN,
    created_at TIMESTAMP
);

INSERT INTO todos (title, complete, created_at) VALUES ('first thing', FALSE, NOW());