const Router = require('koa-router')
const todo = require('./model.js')
const router = new Router()

router.get('/todos', async ctx => {
  const all = await todo.findAll()

  ctx.body = {
    status: 'success',
    message: all,
  }
})

router.get('/todos/:id', async ctx => {
  const result = await todo.findOne(ctx.params.id)

  ctx.body = {
    status: 'success',
    message: result,
  }
})

router.post('/todos/new', async ctx => {
  try {
    await todo.insert(ctx.request.body.title)

    ctx.body = {
      status: 'success',
      title: ctx.request.body.title,
    }
  } catch (error) {
    console.log('error', error)
    ctx.throw(500, 'Error saving todo')
  }
})

module.exports = router
