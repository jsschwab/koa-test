const db = require('../connection')

const todo = {
  findAll: () => db.any('SELECT $1:name FROM todos', ['*']),
  findOne: id => db.one('SELECT * FROM todos where id = $1', [id]),

  insert: title =>
    db.none(
      'INSERT INTO todos(title, complete, created_at) VALUES($1, $2, now())',
      [title, false],
    ),
}

module.exports = todo
