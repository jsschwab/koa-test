const Koa = require('koa')
const app = new Koa()
const bodyParser = require('koa-bodyparser')
const indexRoutes = require('./routes/index')
const todoRoutes = require('./todo/routes')

app.use(async (ctx, next) => {
  await next()
  const responseTime = ctx.response.get('X-Response-Time')
  console.log(`${ctx.method} ${ctx.url} - ${responseTime}`)
})

app.use(async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
  ctx.set('X-Response-Time', `${ms}ms`)
})

app.use(bodyParser())
app.use(indexRoutes.routes())
app.use(todoRoutes.routes())

app.listen(3000)
